package com.ice_os.android.pound_a_politician;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class GameCore extends Thread{

	final static public int LOWERBOUND = 0;
	final static public int UPPERBOUND = 5;

	private int upperBound;
	private int WaitTime;
	private Handler handler;

	public enum GameState{RUNNING, STOPPED};
	private GameState gameState;
	
	public GameCore(Handler handler){
		super();
		this.upperBound = UPPERBOUND;
		gameState = GameState.RUNNING;
		this.handler=handler;
		this.WaitTime=1000;
	}
	@Override	
	public void run(){
		while (gameState == GameState.RUNNING){
			int newPosition;
			newPosition =(int) (Math.random()*(upperBound+1));
			nextStep(newPosition);
			try {
				sleep(WaitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 			

			
		}
	}
	private void nextStep(int newPosition){
		Message message = handler.obtainMessage();
		Bundle b = new Bundle();
		b.putInt("newPosition", newPosition);
		message.setData(b);
		handler.sendMessage(message);
	}
	
	public int GetUpperBound() {
		return upperBound;
	}

	public void SetUpperBound(int UpperBound) {
		this.upperBound = UpperBound;
	}

	public String GetGameState() {
		return gameState.name();
	}

	public void SetGameState(String state) {
		if (state.equals(GameState.RUNNING.name()))	this.gameState = GameState.RUNNING;
		else this.gameState = GameState.STOPPED;
			
	}
	
	
	public synchronized void StopThread() {
		this.gameState = GameState.STOPPED;
		
	}
	
	public void SetWaitTime(int WaitTime){
		this.WaitTime=WaitTime;
		
	}
}
