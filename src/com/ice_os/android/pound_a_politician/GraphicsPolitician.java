package com.ice_os.android.pound_a_politician;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


public class GraphicsPolitician extends BaseAdapter implements View.OnClickListener {
	private Context politicianContext;

	public GraphicsPolitician(Context c) {
		politicianContext = c;
	}

	@Override
	public boolean areAllItemsEnabled() {

		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		return super.isEnabled(position);
	}

	public int getCount() {
		return PoliticianThumbIDs.length;
	}

	public Integer getItem(int position) {

		Integer item = 0;

		for (int i = 0; i < getCount(); i++) {
			if (PoliticianThumbIDs[i].equals(item)) {
				item = PoliticianThumbIDs[i];
			}
		}

		return item;
	}

	public void setItem(Integer index, Integer item) {
		PoliticianThumbIDs[index] = item;
	}

	public long getItemId(int position) {

		long item = 0;

		for (int i = 0; i < getCount(); i++) {
			if (PoliticianThumbIDs[i].equals(position)) {
				item = PoliticianThumbIDs[i];
			}
		}
		return item;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { 
			imageView = new ImageView(politicianContext);
			imageView.setLayoutParams(new GridView.LayoutParams(128, 128));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(3, 3, 3, 3);
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(PoliticianThumbIDs[position]);

		return imageView;
	}

	
	private Integer[] PoliticianThumbIDs = { R.drawable.podium, R.drawable.podium,
			R.drawable.podium, R.drawable.podium, R.drawable.podium, R.drawable.podium

	};

	public Integer[] getPoliticianThumbIDs() {

		return PoliticianThumbIDs;
	}

	public void onClick(View v) {
		
		this.notifyDataSetChanged();

	}

}