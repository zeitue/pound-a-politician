package com.ice_os.android.pound_a_politician;

import java.lang.reflect.Field;
import java.util.Random;
import com.ice_os.android.pound_a_politician.R.drawable;
import com.ice_os.android.pound_a_politician.R.string;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */
	private int CurrentScore = 0;
	private int CurrentLevel = 1;
	private int CurrentLives = 5;
	private static int currentPoliticianLocation = -1;
	private Handler step;
	private Handler Update;
	private GameCore GC;
	private static boolean IsPolitician = true;
	private static GraphicsPolitician GPolitician;
	static int[] PoliticiansArray = new int[6];
	int[] SayingsArray = new int[6];
	static int current;
	static boolean life;
	static double probability = 0.95;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		for (int num = 1; num < 7; num++) {

			String icon = sp.getString("listPref" + num, "obama");
			life = sp.getBoolean("lifeMode", false);
			try {
				Class<drawable> res = R.drawable.class;
				Field field = res.getField(icon);
				PoliticiansArray[num] = field.getInt(null);
				Class<string> stringRes = R.string.class;
				Field stringField = stringRes.getField(icon);
				SayingsArray[num] = stringField.getInt(null);

			} catch (Exception e) {
				Log.e("MyTag", "Failed", e);
			}

		}
		final TextView ScoreTextView = (TextView) findViewById(R.id.Score);
		final TextView LivesTextView = (TextView)findViewById(R.id.Lives);
		final TextView LevelTextView = (TextView)findViewById(R.id.Level);
		final GridView PodiumGrid = (GridView) findViewById(R.id.grid);
		final TextView SayingView = (TextView)findViewById(R.id.sayingView);
		if(life == false){
			LivesTextView.setVisibility(TextView.INVISIBLE);
			LevelTextView.setVisibility(TextView.INVISIBLE);
		}
		SayingView.setText("");
		step = new ChangeImage();
		Update = new Handler();
		GPolitician = new GraphicsPolitician(this);
		PodiumGrid.setAdapter(GPolitician);

		PodiumGrid.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, final View v,
					int position, long id) {
				if (currentPoliticianLocation == position) {
					Update.post(new Runnable() {

						public void run() {
							if(life == false)
							{
							if (IsPolitician == true) {
								CurrentScore++;
								ScoreTextView.setText("Score: " + CurrentScore);
								ScoreTextView.refreshDrawableState();
								SayingView.setText(SayingsArray[current]);
								SayingView.refreshDrawableState();
							
								if (CurrentScore == 10) {
									GC.SetWaitTime(800);
								} else if (CurrentScore == 30) {
									GC.SetWaitTime(600);
								} else if (CurrentScore == 60) {
									GC.SetWaitTime(400);
								}
							}}
							else {
								if (IsPolitician == true) {
									CurrentScore++;
									ScoreTextView.setText("Score: " + CurrentScore);
									ScoreTextView.refreshDrawableState();
									SayingView.setText(SayingsArray[current]);
									SayingView.refreshDrawableState();
								if (CurrentScore == 10) {
									GC.SetWaitTime(1200);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.90);
								} else if (CurrentScore == 20) {
									GC.SetWaitTime(1000);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.85);
								} else if (CurrentScore == 30) {
									GC.SetWaitTime(900);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.80);
								}
								else if (CurrentScore == 40) {
									GC.SetWaitTime(800);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.70);
								}
								else if (CurrentScore == 50) {
									GC.SetWaitTime(750);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.65);
								}
								else if (CurrentScore == 60) {
									GC.SetWaitTime(700);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.60);
								}
								else if (CurrentScore == 70) {
									GC.SetWaitTime(600);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.55);
								}
								else if (CurrentScore == 80) {
									GC.SetWaitTime(500);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.50);
								}
								else if (CurrentScore == 100) {
									GC.SetWaitTime(400);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.45);
								}
								else if (CurrentScore == 110) {
									GC.SetWaitTime(350);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.40);
								}}
							else
							{
								CurrentLives--;
								LivesTextView.setText("Lives: " + CurrentLives);
								LivesTextView.refreshDrawableState();
								if(CurrentLives == 0)
								{
									GC.StopThread();
									Intent gameOverIntent = new Intent(MainActivity.this,GameOver.class);
									startActivity(gameOverIntent);
									finish();
								}
							}
							}
								
							
						
						
						
						}});}}});
		GC = new GameCore(step);
		GC.start();
	}

	public Activity getActivity() {
		return this.getActivity();
	}

	private static class ChangeImage extends Handler {
		Podium podium = new Podium();
		private int oldLocation = -1;

		@Override
		public void handleMessage(Message message) {
			current = randomNumber(1, 6);
			Bundle bundle = message.getData();
			currentPoliticianLocation = bundle.getInt("newPosition");
			if(life == false){
			GPolitician.setItem(currentPoliticianLocation,
					PoliticiansArray[current]);
			}else if(Math.random() < probability){
				GPolitician.setItem(currentPoliticianLocation,PoliticiansArray[current]);
				IsPolitician = true;
				}
			
			else
			{
				GPolitician.setItem(currentPoliticianLocation,R.drawable.life);
				IsPolitician = false;
			}
			if (oldLocation != -1 && currentPoliticianLocation != oldLocation) {
				GPolitician.setItem(oldLocation, podium.getPodium());
			}

			oldLocation = currentPoliticianLocation;
			GPolitician.notifyDataSetChanged();

		}

	}

	public static int randomNumber(int min, int max) {
		return min + (new Random()).nextInt(max - min);
	}

	public void setProbability(double probability){
		MainActivity.probability=probability;
	}

}